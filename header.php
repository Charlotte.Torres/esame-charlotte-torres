<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title><?php bloginfo('title'); ?></title>
	<link rel="icon" href="img/Fevicon.png" type="image/png">

  <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
  <!--================Header Menu Area =================-->
  <header class="header_area">
    <div class="main_menu">
      <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <a class="navbar-brand logo_h" href="index.html"><img src="<?php bloginfo('template_directory'); ?>/img/logo.png" alt=""></a>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="navbar-collapse" id="navbarSupportedContent">

            <?php get_template_part( 'template-parts/nav-menu' );?>

          </div>
        </div>
      </nav>
    </div>
  </header>
