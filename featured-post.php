<?php

$args = array(
	'posts_per_page' => 1, // how many posts.
);

$featured_post = new WP_Query( $args );

if ( $featured_post->have_posts() ) {
	while ( $featured_post->have_posts() ) {
		$featured_post->the_post();
		// Save Post ID.
		$featured_id = $post->ID;
		// Codice HTML per mostrare il Post.
		?>
      <div class="container">
        <div class="hero-banner">
          <div class="hero-banner__content">
            <h3 id="categoria_principale"><?php the_category(); ?></h3>
						<a href="<?php the_permalink(); ?>">
							<h1 id="titolo_principale"><?php the_title(); ?></h1>
						</a>
						<div class="">
							<time><?php the_time( 'F j, Y'); ?></time>
						</div>

            <?php
            /**
             * @see https://wordpress.org/support/article/formatting-date-and-time/
             */
            ?>

          </div>
        </div>
      </div>
		<?php
	} // End loop.
} // End if.
