<?php
/**
 * Template functions.
 *
 * @author Claudio Salatino <claudio@wordlift.io>
 * @since 1.0.0
 */


// Custom function to display Post thumbnail.
function esame_post_thumbnail( $size ) {

	if ( is_single() ) {
	    ?>
        <figure class="bleed">
			<?php the_post_thumbnail( $size ); ?>
            <figcaption>This is a caption set in <a href="https://fonts.google.com/specimen/Montserrat"
                                                    target="_blank">Montserrat</a></figcaption>
        </figure>
		<?php

	} else {

		the_post_thumbnail( $size );
	}


}
