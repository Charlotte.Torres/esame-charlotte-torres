<?php
/**
 * Sidebar.
 */
?>

      <div class="single-sidebar-widget newsletter-widget">
        <aside class="main-sidebar">
            <?php
        	if ( function_exists( 'dynamic_sidebar' ) ) {
        		dynamic_sidebar( 'right-sidebar' );
        	}
        	?>
        </aside>
      </div>
