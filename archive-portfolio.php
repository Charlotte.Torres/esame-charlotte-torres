<?php get_header(); ?>
<main class="site-content">
  <section class="mb-30px">
    <div class="container">
      <div class="hero-banner hero-banner--sm">
        <div class="hero-banner__content">
          <h1><?php the_archive_title(); ?></h1>
        </div>
      </div>
    </div>
  </section>
  <section class="blog-post-area section-margin">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <div class="row">
            <?php if ( have_posts() ) : ?>
              <?php while ( have_posts() ) :
                the_post();?>
                <div class="col-md-6">

                  <?php  get_template_part('template-parts/content/content', 'portfolio') ?>

                </div>
              <?php endwhile; ?>


      			<?php endif; ?>
          </div>
        </div>
        <div class="col-lg-4 sidebar-widgets">
          <div class="widget-wrap">
            <div class="single-sidebar-widget">
              <?php get_sidebar();?>
            </div>
          </div>
        </div>
      </div>
    </div>
      <div class="row">
        <div class="col-lg-12">
          <nav class="blog-pagination justify-content-center d-flex">
            <ul class="pagination">
              <li class="page-item">
                <?php the_posts_pagination(
                  array(
                    'prev_text' => sprintf( '<span class="previous-posts">%s</span>',
                      __( 'Previous', 'esame' )
                    ),

                    'next_text' => sprintf( '<span class="next-posts">%s</span>',
                      __( 'Next', 'esame' )
                    ),
                  )
                ); ?>
              </li>
            </ul>
          </nav>
        </div>
        </div>
      </div>

  </section>
</main>

<?php get_footer(); ?>
