
  <div class="single-recent-blog-post">
    <div class="thumb card-img">

      <?php
          // Display excerpt only in Archive.
  		if ( is_front_page() ||  is_home() ) {
  			the_post_thumbnail('full');
  		}
      else {
        the_post_thumbnail('medium');
      }
  		?>
      <ul class="thumb-info">
        <li><a href="#"><i class="ti-user"></i><?php the_author(); ?></a></li>
        <li><a href="#"><i class="ti-notepad"></i><?php the_time('F, j, Y'); ?></a></li>
      </ul>
    </div>
    <div class="details mt-20">
      <a href="<?php the_permalink(); ?>">
        <h3><?php the_title(); ?></h3>
      </a>
      <?php
          // Display excerpt only in Archive.
  		if ( ! is_front_page() || ! is_home() ) {
  			the_excerpt();
  		}
  		?>

      <?php if ( get_the_terms( get_the_ID(), 'portfolio_categories' ) ) {
        the_terms( get_the_ID(), 'portfolio_categories');
      }?>
      <a class="button" href="<?php the_permalink(); ?>"><?php _e('ReadMore', 'esame'); ?> <i class="ti-arrow-right"></i></a>
    </div>
  </div>
