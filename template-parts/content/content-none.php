<?php
/**
 * Content not found.
 */
?>

<main class="site-content">

    <section class="mb-30px">

        <header class="blog-entry__header">
            <h1 class="blog-entry__header__title">Not Found</h1>
        </header>

        <p><?php esc_html_e('Sorry, but we can\'t find what you are looking for. Try searching using the form below.', 'esame') ?></p>
		<?php get_search_form(); ?>
  </section>
</main>
