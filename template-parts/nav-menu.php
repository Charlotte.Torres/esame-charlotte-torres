<?php
if ( function_exists( 'wp_nav_menu' ) && has_nav_menu( 'main-menu' ) ) {
  wp_nav_menu( array(
    'container' => 'header_area',
    'theme_location'  => 'main-menu',
    'menu_class' => 'nav menu_nav'

  ) );
}
