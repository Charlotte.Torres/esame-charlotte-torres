<?php get_header();?>
<main class="site-main">
  <!--================Hero Banner start =================-->
  <section class="mb-30px">
    <?php
    // Only on the Homepage.
    if ( is_front_page() ) {
      include TEMPLATEPATH . '/featured-post.php';
    }
    ?>
  </section>
  <!--================Hero Banner end =================-->

  <!--================ Start Blog Post Area =================-->
  <section class="blog-post-area section-margin mt-4">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
        <?php if ( have_posts() ) : ?>
          <?php while ( have_posts() ) :
            the_post();
            if ( $post->ID !== $featured_id ) :
              get_template_part('template-parts/content/content-excerpt', 'excerpt') ?>
          <?php endif; endwhile; ?>

        <?php else:
          // If there aren't any Posts.
          get_template_part( 'template-parts/content/content', 'none' );


        endif;

        ?>


          <div class="row">
            <div class="col-lg-12">
                <nav class="blog-pagination justify-content-center d-flex">
                    <ul class="pagination">
                        <?php the_posts_pagination(
                					array(
                						'prev_text' => sprintf( '<span class="older-posts">%s</span>',
                							__( 'Older', 'esame' )
                						),

                						'next_text' => sprintf( '<span class="newer-posts">%s</span>',
                							__( 'Next', 'esame' )
                						),
                					)
                				); ?>
                    </ul>
                </nav>
            </div>
          </div>
        </div>

        <!-- Start Blog Post Siddebar -->
        <div class="col-lg-4 sidebar-widgets">
          <div class="widget-wrap">
            <div class="single-sidebar-widget">
              <?php get_sidebar();?>
            </div>
          </div>
        </div>
      </div>
        <!-- End Blog Post Siddebar -->
      </div>
  </section>
  <!--================ End Blog Post Area =================-->
</main>
<?php get_footer(); ?>
