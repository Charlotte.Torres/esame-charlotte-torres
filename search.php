<?php
/**
 * Display search results.
 */
get_header(); ?>

    <main class="site-content">

        <section class="mb-30px">
          <div class="container">
            <div class="hero-banner">
              <div class="hero-banner__content">
					<?php
					// Search results.
					if ( is_search() ) {

					global $wp_query;

					$page_heading = sprintf(
						'%1$s %2$s',
						'<h3>' . __( 'Search results for', 'esame' ),
						'&ldquo;' . get_search_query() . '&rdquo;</h3>'
					);

					if ( $wp_query->found_posts ) {

						$page_subheading = sprintf(
							_n(
								'We found %s post for your search',
								'We found %s posts for your search',
								$wp_query->found_posts,
								'tema-ied'
							),
							number_format_i18n( $wp_query->found_posts )

						);
					} else {
						$page_subheading = __( 'Sorry, but there are no results for your search query. Try searching again using the form on the right or go back to the <a href="' . esc_url( home_url( '/' ) ) . '">Homepage</a>', 'tema-ied' );
					}

					if ( $page_heading && $page_subheading ) {
						echo $page_heading . '<br>' . $page_subheading;
					}
					?>
        </div>
      </div>

				<?php

				}
        if ( have_posts() ) : ?>
          <div class="mb-30p">
      			<?php while ( have_posts() ) : the_post();?>
              <div class="container">
                <div class="hero-banner">
                  <div class="hero-banner__content">
                    <h3><?php the_category(); ?></h3>
                    <h1 <?php the_permalink(); ?>><?php the_title(); ?></h1>
                    <time><?php the_time( 'F j, Y'); ?></time>
                  </div>
                </div>
              <div class="details mt-20">
                <p class="tag-list-inline"><?php the_tags(); ?> </p>
                <p><?php the_content(); ?></p>
            </div>
            </div>
      				<?php get_sidebar();

      			endwhile;
      			?>
              </div>
      	<?php endif; ?>

			<?php get_sidebar(); ?>
        </div>
      

    </main>

<?php get_footer();
