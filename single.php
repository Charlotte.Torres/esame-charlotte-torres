<?php get_header(); ?>

<main class="site-main">
	<?php if ( have_posts() ) : ?>
    <div class="mb-30p">
			<?php while ( have_posts() ) : the_post();?>
        <div class="container">
          <div class="hero-banner">
            <div class="hero-banner__content">
              <h3><?php the_category(); ?></h3>
              <h1 <?php the_permalink(); ?>><?php the_title(); ?></h1>
              <time><?php the_time( 'F j, Y'); ?></time>
            </div>
          </div>
        <div class="details mt-20">
					<div class="row">
						<div class="col-lg-8">
		          <p class="tag-list-inline"><?php the_tags(); ?> </p>

		          <p>

								<?php the_content();?>
								<?php echo get_post_meta($post->ID, 'name', true); ?>

								<?php wp_link_pages( array(
      							'before' => '<div>' . __( 'Pages:', 'esame' ),
      							'after'  => '</div>',
      						)
      					);?>
						</p>

						</div>



					<?php endwhile; ?>

			<?php endif; ?>

	<div class="col-lg-4 sidebar-widgets">
		<div class="widget-wrap">
			<div class="single-sidebar-widget">
				<?php get_sidebar();?>
			</div>
		</div>
	</div>
	</div>
	</div>
</div>
</div>

	<!-- End Blog Post Siddebar -->

</main>

<?php get_footer(); ?>
