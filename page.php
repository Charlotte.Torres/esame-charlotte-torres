<?php get_header(); ?>

  <!--================ Hero sm Banner start =================-->
  <section class="mb-30px">
    <div class="container">
      <div class="hero-banner hero-banner--sm">
        <div class="hero-banner__content">
          <h1><?php the_title(); ?></h1>
        </div>
      </div>
    </div>
  </section>
  <!--================ Hero sm Banner end =================-->




  <!--================ Start Blog Post Area =================-->
  <section class="blog-post-area section-margin">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <div class="main_blog_details">
            <img class="img-fluid" src="img/blog/blog4.png" alt="">
            <a href="#"><h4><?php the_title(); ?></h4></a>
            <div class="user_details">
              <div class="float-left">
                <?php the_category(); ?>
              </div>
              <div class="float-right mt-sm-0 mt-3">
                <div class="media">
                  <div class="media-body">
                    <h5><?php the_author(); ?></h5>
                    <p><?php the_time('F j, Y'); ?></p>
                  </div>
                  <div class="d-flex">
                    <?php the_post_thumbnail('full'); ?>
                  </div>
                </div>
              </div>
            </div>
            <?php while (have_posts()) : the_post(); ?>
            <p><?php the_content(); ?></p>
             <?php endwhile; ?>
          </div>

        <!--================ Start Blog Post Area =================-->
                      
              </div>

              <!-- Start Blog Post Siddebar -->
              <div class="col-lg-4 sidebar-widgets">
            		<div class="widget-wrap">
            			<div class="single-sidebar-widget">
            				<?php get_sidebar();?>
            			</div>
            		</div>
            	</div>
            </div>
              <!-- End Blog Post Siddebar -->
            </div>
        </section>

  <!--================ End Blog Post Area =================-->
  <?php get_footer(); ?>
